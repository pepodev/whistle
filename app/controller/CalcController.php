<?php

use core\Controller;
use core\Database;
use core\Filter;
use core\Redis;


class CalcController extends Controller{


    public function build()
    {

        $code_set = 0;

        $country_name = '';

        $country_code = '';

        if(isset($_POST['code']) && !empty($_POST['code']))
        {
            $posted_code_uri ='calculadora-provincial/'.$_POST['code'];
            header('Location: /'.$posted_code_uri);

        }else{

            $params = $this->getParams();

            if(isset($params['params'][1]) && !empty($params['params'][1]))
            {
                $code_set = 1;

                $country_code_2 = substr($params['params'][1] ,0,2);

                $country_code = $params['params'][1];

                $cache_definition = $this->getModelCacheDefinition();

                $cache_definition['params'] = $country_code_2;

                $db_result = Database::getCacheOrQueryDb( $cache_definition , $this->cache);

                if(isset($db_result) && !empty($db_result))
                {
                    $redis = new Redis();

                    $country_name = $db_result['provincia'];

                    $redis->incrementKeyCount( 'zip_searchs','1', $country_code );

                }else{
                    $country_name = 'no hay ninguna provincia con este codigo postal';
                }
            }
        }
        $this->renderTwig (
            'calculadora.html.twig',
            array (
                'postal_code' => $country_name,
                'code_set' => $code_set,
                'country_code' => $country_code,
                'server_name'=> $_SERVER['SERVER_NAME']
            )
        );
    }
    public function getModelCacheDefinition()
    {
        $cache_definition['method'] = 'getProvincia';
        $cache_definition['model'] = '\app\model\CalcModel';
        $cache_definition['params'] = null;
        $cache_definition['ttl'] = 5;

        return $cache_definition;
    }

}