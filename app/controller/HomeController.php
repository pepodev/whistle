<?php

use core\Controller;
use core\Database;
use core\Redis;

class HomeController extends Controller
{

    /***
     * assigns all the variables that the view needs
     * and indicates the template file
     */
    public function build ()
    {

        $cache_definition = $this->getModelCacheDefinition();

        $provincias = Database::getCacheOrQueryDb( $cache_definition , $this->cache);

        $x = 0;

        $redis = new Redis();

        $top_results = $redis->getTopResults('zip_searchs' ,'0','9');

        foreach($provincias as $provincia)
        {
            $provincias[$x]['url'] =  rawurlencode($provincia['provincia']);
            $x++;
        }

        $this->renderTwig (
            'home.html.twig',
            array (
                'provincias' => $provincias,
                'top_results'=>$top_results,
                'server_name'=> $_SERVER['SERVER_NAME']
            )
        );
    }
    public function getControllerCacheDefinition()
    {
        $definition = __CLASS__;
        $ttl=10;

        return[$definition,$ttl];
    }
    public function getModelCacheDefinition()
    {
        $cache_definition['method'] = 'getProvincias';
        $cache_definition['model'] = '\app\model\HomeModel';
        $cache_definition['params'] = null;
        $cache_definition['ttl'] = 30;

        return $cache_definition;
    }
}