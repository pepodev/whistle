<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 23/03/14
 * Time: 03:16
 */
use core\Controller;
use core\Filter;
use app\model\MunicipiosModel;


class SearchController extends Controller{

    public function build()
    {

        $model = new MunicipiosModel();

        //todo check if isset

        $page = $_GET['page'];

        $municipio = $_GET['query'];

        $prev_page = $page -1;

        $next_page = $page +1;

        $municipios = $model->searchMunicipios( $municipio , $page ,0 );

        $error_querying_db = 0;

        if(isset($municipios[0]))
        {
            if($municipios[0] === '00000')
            {
                $error_querying_db = 1;
            }
        }

        $total_municipios = $model->getAllMunicipiosSearch($municipio);

        $url = '/search/?query='.$municipio.'&page=';

        $this->renderTwig (
            'search.html.twig',
            array (
                'provincia' => $municipio,
                'municipios' => $municipios,
                'page'=> $page,
                'total_pages'=>$total_municipios,
                'next_page'=>$url.$next_page,
                'prev_page'=>$url.$prev_page,
                'url'=>$url,
                'error_querying_db' => $error_querying_db,
                'server_name'=> $_SERVER['SERVER_NAME']

            )
        );
    }

}