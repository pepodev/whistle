<?php

use core\Controller;
use core\Filter;
use app\model\MunicipiosModel;
use core\Sphinx;


class MunicipiosController extends Controller{


    public function build()
    {
        $sphinx = new Sphinx();

        $params = $this->getParams();

//        $model = new MunicipiosModel();

        //todo check if isset

        $provincia = rawurldecode($params['params'][1]['param']);

        $page = $params['params'][1]['param_parts'][0];

        $prev_page = $page -1;

        $next_page = $page +1;

        $results = $sphinx->find( $provincia , 'municipios' , $page);

//      $municipios = $model->getMunicipios( $provincia , $page );

//      $total_municipios = $model->getAllMunicipios($provincia);

        $total_municipios = $results[1];

//        var_dump($results[1]);

        $url = '/'.$params['params'][0].'/'.$params['params'][1]['param'].':';

        $this->renderTwig (
            'municipios.html.twig',
            array (
                'provincia' => $provincia,
                'municipios' => $results[0],
//                'municipios' => $municipios,
                'page'=> $page,
                'total_pages'=>$total_municipios,
                'next_page'=>$url.$next_page,
                'prev_page'=>$url.$prev_page,
                'url'=>$url,
                'server_name'=> $_SERVER['SERVER_NAME']
            )
        );
    }

    public function getControllerCacheDefinition()
    {
        $params = $this->getParams();

        $provincia = rawurldecode($params['params'][1]['param']);

        $page = $params['params'][1]['param_parts'][0];

        $definition = __CLASS__.$provincia.$page;
        $ttl=300;

        return[$definition,$ttl];
    }

}