<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 12/04/14
 * Time: 19:47
 */

use core\Controller;
use core\Filter;
use app\model\MunicipiosModel;



class TypeaheadController  extends Controller{

    public function build()
    {
        $model = new MunicipiosModel();

        $municipio = $_POST['search'];

        $municipios = $model->searchMunicipios( $municipio , 1 , 1);

        $this->renderJson($municipios);

    }

}