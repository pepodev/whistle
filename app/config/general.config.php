<?php
$config['framework.local']= array(
    'DefaultLanguage'=>'es_ES',
    'Languages'=> array("es_ES","en_US"),
    'Database'=> array(
        "host"=>"localhost",
        "db_name"=>"sphinx_demo",
        "user"=>"root",
        "password"=>"",
                    ),
    'Environment' => 'prod',
    'Url'=> array(
        "url_separator"=>"/",
        "context_separator"=>":"
     ),
    'Cache' => array(
        "active"=>1,
        "type"=>"memory"
    )
);
$config['framework.josep']= array(
    'DefaultLanguage'=>'es_ES',
    'Languages'=> array("es_ES","en_US"),
    'Database'=> array(
            "host"=>"localhost",
            "db_name"=>"sphinx_demo",
            "user"=>"root",
            "password"=>"",
        ),
    'Environment' => 'dev',
    'Url'=> array(
                "url_separator"=>"/",
                "context_separator"=>":"
     ),
    'Cache' => array(
        "active"=>1,
        "type"=>"memory"
    )
 );
