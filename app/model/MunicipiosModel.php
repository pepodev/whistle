<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 01/02/14
 * Time: 12:34
 */
namespace app\model;

use core\Database;
use \PDO;


class MunicipiosModel extends Database{

    //model example
    public function __construct()
    {
        parent::__construct();

    }
    public function getMunicipios( $provincia , $page ){


        $to = $page * 10;
        $from = (int)($to - 10);
        if($from>=10)
        {
            $from-=1;
        }


        $query = "SELECT t2.*
                  FROM provincias t1
                  INNER JOIN municipios t2 USING(id_provincia)
                  WHERE provincia LIKE :provincia ORDER BY nombre LIMIT :from , 10";

        $statement = $this->connection->prepare( $query );
        $statement->bindParam( ':provincia', $provincia , PDO::PARAM_STR );
        $statement->bindParam( ':from', $from , PDO::PARAM_INT );
        $statement->execute();

        $result = $statement->fetchAll();

        if( isset($result) && !empty($result))
        {
            return $result;

        }else{

            return $this->connection->errorInfo();

        }
    }
    public function getAllMunicipios($provincia){


        $query = "SELECT count(id_municipio) AS nums_municipio
                  FROM provincias t1
                  INNER JOIN municipios t2 USING(id_provincia)
                  WHERE provincia LIKE :provincia";
        $statement = $this->connection->prepare( $query );
        $statement->bindParam( ':provincia', $provincia , PDO::PARAM_STR );
        $statement->execute();

        $result = $statement->fetchAll();

        if( isset($result) && !empty($result))
        {
            $total_municipios = $result[0]['nums_municipio'];

            $pages = $total_municipios/10;

            if(is_float($pages))
            {
                $pages = ((int)$pages)+1;
            }

            return $pages;

        }else{

            return $this->connection->errorInfo();

        }
    }
    public function searchMunicipios($municipio , $page , $isjson){

        $to = $page * 10;
        $from = (int)($to - 10);
        if($from>=10)
        {
            $from-=1;
        }

        $municipio = '%'.$municipio.'%';
        $query = "SELECT nombre
                  FROM municipios
                  WHERE nombre LIKE :municipio
                  ORDER BY nombre LIMIT :from , 10";
        $statement = $this->connection->prepare( $query );
        $statement->bindParam( ':municipio', $municipio, PDO::PARAM_STR );
        $statement->bindParam( ':from', $from, PDO::PARAM_INT );
        $statement->execute();

        $results = $statement->fetchAll();

        if( isset($results) && !empty($results))
        {
            if($isjson)
            {
                foreach($results as $result)
                {
                    $return[] = $result['nombre'];
                }
                return $return;
            }
            return $results;

        }else{

            //todo if dev
//            return $this->connection->errorInfo();
            return array();

        }
    }
    public function getAllMunicipiosSearch($municipio){

        $municipio = '%'.$municipio.'%';

        $query = "SELECT count(id_municipio) AS nums_municipio
                  FROM municipios
                  WHERE nombre LIKE :municipio";
        $statement = $this->connection->prepare( $query );
        $statement->bindParam( ':municipio', $municipio , PDO::PARAM_STR );
        $statement->execute();

        $result = $statement->fetchAll();

        if( isset($result) && !empty($result))
        {
            $total_municipios = $result[0]['nums_municipio'];

            $pages = $total_municipios/10;

            if(is_float($pages))
            {
                $pages = ((int)$pages)+1;
            }

            return $pages;

        }else{

            return $this->connection->errorInfo();

        }
    }
} 