<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 01/02/14
 * Time: 12:34
 */
namespace app\model;

use core\Database;
use \PDO;


class CalcModel extends Database{


    public function getProvincia($id){

        $query = "SELECT provincia FROM provincias WHERE id_provincia=:id ";

        $statement = $this->connection->prepare( $query );
        $statement->bindParam( ':id', $id , PDO::PARAM_INT );
        $statement->execute();

        $result = $statement->fetchAll();

        if( isset($result) && !empty($result))
        {
            return $result[0];

        }else{

            return null;

        }
    }}