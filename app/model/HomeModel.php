<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 01/02/14
 * Time: 12:34
 */
namespace app\model;

use core\Database;
use \PDO;


class HomeModel extends Database{

    //model example
    public function getProvincias(){


        $query = "SELECT provincia FROM provincias ";

        $statement = $this->connection->prepare( $query );
        $statement->execute();

        $result = $statement->fetchAll();

//        var_dump('llamo metodo get provincias');

        if( isset($result) && !empty($result))
        {
            return $result;

        }else{

            return $this->connection->errorInfo();

        }
    }
} 