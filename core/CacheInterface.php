<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 19/03/14
 * Time: 19:27
 */

namespace core;


interface CacheInterface {

    public function set ( $key , $content , $expiration );

    public function  get ($key);

    public function deleteKey ( $value );

    public function getKey ($parameters);


} 