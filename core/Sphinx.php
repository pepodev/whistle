<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 26/03/14
 * Time: 19:26
 */

namespace core;

include '/var/www/html/sphinx/api/sphinxapi.php';

class Sphinx {

    private $sphinx = null;

    public function __construct()
    {
        $sphinx = new \SphinxClient();
        $sphinx->SetServer( '127.0.0.1', 9312);
        $sphinx->SetMatchMode( SPH_MATCH_EXTENDED );
        $sphinx->SetSortMode( SPH_SORT_RELEVANCE );
        $this->sphinx = $sphinx;
    }
    public function find($query_string , $index , $page)
    {

        $to = $page * 10;
        $from = (int)($to - 10);
        if($from>=10)
        {
            $from-=1;
        }

        $this->sphinx->SetLimits( $from , 10 );

        $query_string = str_replace( "/" , "|" , $query_string );

        $this->sphinx->AddQuery( $query_string, $index );

        $results = $this->sphinx->RunQueries();

        if ( !empty( $results[0]['matches'] ) )
        {
            $pages = $results[0]['total_found']/10;

            if(is_float($pages))
            {
                $pages = ((int)$pages)+1;
            }

            return [$results[0]['matches'],$pages];
        }
        else
        {
            die( "No results found for '$query_string'" );
        }
    }
}
