<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 01/02/14
 * Time: 12:07
 */

namespace core;

use core\Config;
use \PDO;

class Database
{
    protected $connection = null;

    public function __construct()
    {
        $config = Config::getInstance();

        $database = $config->getDatabase;

        $dsn = 'mysql:host='.$database['host'].';dbname='.$database['db_name'];

        try
        {
            $connection = new PDO( $dsn, $database['user'], $database['password'] );
            $connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

            $connection -> exec('SET NAMES utf8'); // METHOD #3
            $connection -> exec('SET CHARACTER SET utf8'); // METHOD #4
            $connection -> exec('SET time_zone = \''.date_default_timezone_get().'\'');

            $environment = Config::getInstance()->getEnvironment;

            if($environment === 'dev')
            {
//                $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//                $connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            }

            $this->connection = $connection;

        }
        catch (PDOException $e)
        {
            $this->connection = null;
            die($e->getMessage());
        }
    }

    public function getError()
    {
        $this->connection->errorInfo();
    }

    public function __destruct()
    {
        $this->connection = null;
    }

    public function __call( $name , array $arguments )
    {

        $method = substr( $name , 0 ,6);
        $property = strtolower(substr( $name ,6 ));

        if($method === 'findBy')
        {
            $table = $arguments[0];

            if(is_string($table)){

                //TODO check property;

                $query = "SELECT * FROM $table WHERE $property=:value";

                $statement = $this->connection->prepare( $query );
                $statement->bindParam( ':value', $arguments[1] , PDO::PARAM_INT );
                $statement->execute();

                $result = $statement->fetch();

                if( isset($result) && !empty($result))
                {
                    return $result;

                }else{

                    return $this->connection->errorInfo();

                }
            }else{

                return 'table param must be string';
            }

        }else{

            throw new Exception('500','Invalid method , dont play with magic noob');
        }

    }

    public static function getCacheOrQueryDb( $cache_definition , $cache )
    {
        $cache_active = Config::getInstance()->getCache['active'];

        if ($cache_active===1)
        {
            if( $cache !== null )
            {
                if($db_data = $cache->get($cache->getKey($cache_definition)))
                {
//                    echo $cache_definition['method'].' loaded from cache, model : ' . $cache_definition['model'];
                    return $db_data;
                }
            }
            $model = new $cache_definition['model']();
            $db_data = $model->$cache_definition['method']($cache_definition['params']);
            $cache->set( $cache->getKey($cache_definition) ,$db_data, $cache_definition['ttl'] );
        }
        $model = new $cache_definition['model']();
        $db_data = $model->$cache_definition['method']($cache_definition['params']);

        return $db_data;

    }
}