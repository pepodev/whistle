<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 30/01/14
 * Time: 15:30
 */

namespace core;


class Config
{

    static protected $config_instance;

    protected $config_path;

    protected $config_values;

    private function __construct(  )
    {

        $this->config_path =  '../app/config/';

        include( $this->config_path .'general.config.php');

        $filter = Filter::getInstance();

        $server_name = $filter->server( 'SERVER_NAME' );

        $this->config_values = $config[$server_name];

    }

    public static function getInstance( )
    {

        if ( !isset( $config_instance ) )
        {
            self::$config_instance = new self();
        }

        return self::$config_instance;

    }

    public function __get( $name )
    {
        $method = substr( $name , 0 , 3 );

        $property = substr( $name , 3 );

        if( $method === 'get' )
        {
            if(isset($this->config_values[$property]))
            {
                return $this->config_values[$property];

            }else{

                throw new Exception( '500','config value not defined');

            }

        }else{

            throw new Exception('500','Magic methods are not that magic ¬¬.');

        }
    }
}