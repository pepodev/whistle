<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 03/02/14
 * Time: 21:36
 */

namespace core;


class Exception extends \Exception{

    public function __construct( $http , $message )
    {
        $environment = Config::getInstance()->getEnvironment;

        switch( $http )
        {
            case'403': header ('HTTP/1.1 403 Forbidden');
                echo'';
                break;
            case'404': header ('HTTP/1.1 404 Not Found');
                echo'';
                break;
            case'500': header ('HTTP/1.1 500 Internal Server Error');
                echo'';
                break;
        }

        if( $environment === 'dev')
        {

            $html = "<audio src=\"whistle.ogg\" autoplay></audio><div id=\"display\"></div><script type=\"text/javascript\">var audio = document.getElementsByTagName(\"audio\")[0];var display = document.getElementById(\"display\");audio.addEventListener(\"MozAudioAvailable\", writeSamples, false);function writeSamples (event) {display.innerHTML += event.frameBuffer[0] + ', ';};</script>";

            echo 'Header:' . $http .'<br/>Message:' . $message;
            //echo $html;

        }elseif( $environment === 'prod' ){

        }

    }
} 