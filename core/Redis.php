<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 07/04/14
 * Time: 10:07
 */

namespace core;


class Redis {

    private $client;

    function __construct (  )
    {
        $this->client = new \Predis\Client(
            array(
                'scheme'=> 'tcp',
                'host'=> '127.0.0.1',
                'port'=> '6379',
            )
        );

    }

    public function set ( $key , $content )
    {
        $this->client->set( $key , $content );
    }

    public function incrementKeyCount($key , $increment , $member )
    {
        $time = time();
        $week  = date('W', $time);
        $year  = date('Y', $time);
        $key.= '_' . $week . '_' . $year;
        $this->client->ZINCRBY( $key , $increment , $member );
    }

    public function getTopResults( $group , $from , $to )
    {
        $time = time();
        $week  = date('W', $time);
        $year  = date('Y', $time);
        $group.= '_' . $week . '_' . $year;

        $top_results = $this->client->zrevrangebyscore( $group, $to ,$from ,'withscores');

        return $top_results;

    }

    public function get ($key)
    {
//        //todo check if has expired
//        if( $this->is_cache_active === 0 )
//        {
//            return null;
//        }
        return $this->client->get( $key );
    }

} 