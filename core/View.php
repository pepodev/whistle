<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 15/01/14
 * Time: 19:40
 */

namespace core;



class View {

    private $vars = array();
    private $output;
    private $twig;

    public function __construct()
    {

        $config = Config::getInstance();

        $environment = $config->getEnvironment;

        $loader = new \Twig_Loader_Filesystem('../app/view/');

        if( $environment === 'prod')
        {
            $twig = new \Twig_Environment( $loader , array(
                'cache' => '../app/view/compilation_cache'
            ));

        }elseif( $environment === 'dev' ){

            $twig = new \Twig_Environment( $loader );

        }

        $this->twig = $twig;

    }

    public function assignPhp( $vars )
    {

        $this->vars = $vars;

    }

    public function renderPhp( $template )
    {

            ob_start();
            include('../app/view/'.$template);
            $this->output = ob_get_clean();

    }
    public function renderTwig( $page , $vars)
    {

        $this->output = $this->twig->render( $page , $vars );

    }

    public function renderJson( $json )
    {
        header('Content-type: application/json');
        $this->output = $json;
    }

    public function show()
    {

        echo $this->output;
    }

    public function getHtml()
    {
        return $this->output;
    }

} 