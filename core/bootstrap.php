<?php


namespace core;

use core\cache\Disk;
use core\View;
use core\Router;
use core\cache\Memory;

class Bootstrap {

    //private $uriParams = '';
    private $controller = '';

    public function __construct()
    {
        require_once '../autoload.php';
        require_once '../vendor/twig//twig/lib/Twig/Autoloader.php';
        \Twig_Autoloader::register();
        //TO
    }

    public function execute()
    {
        try{

            $router = new Router();
            $this->controller = $router->constructPath().'Controller';

            $controllerFileName = ucfirst($this->controller);

            //todo instantiate type of cache defined in config file

            $cache_active = Config::getInstance()->getCache['active'];

            $controller = New $controllerFileName( new View() );

            if ($cache_active===1)
            {
                $cache_type = strtolower(Config::getInstance()->getCache['type']);

                if( $cache_type === 'memory')
                {
                    $cache = new Memory();
                }elseif( $cache_type === 'disk'){
                    $cache = new Disk();
                }

                $controller->setCache($cache);
            }

            $controller->execute();

        }catch(Exception $e){

            echo $e->getMessage();

        }
    }
}