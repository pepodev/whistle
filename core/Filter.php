<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 30/01/14
 * Time: 15:30
 */

namespace core;


class Filter
{

    static protected $filter_instance;

    protected  $filter_values = array();

    private function __construct(  )
    {

        $this->filter_values['post'] = $_POST;
        $this->filter_values['server'] = $_SERVER;
        $this->filter_values['get'] = $_GET;
        $this->filter_values['cookie'] = $_COOKIE;
        $this->filter_values['request'] = $_REQUEST;

    }

    public static function getInstance( )
    {

        if ( !isset( $filter_instance ) )
        {
            self::$filter_instance = new self();
        }

        return self::$filter_instance;

    }

    public function __call( $name , array $arguments )
    {

        $to_query = $arguments[0];

        $bind = (isset($arguments[1]) ? $arguments[1] : false);

        if( $name === 'post' )
        {
            if( isset($this->filter_values['post'][$to_query]) && !empty($this->filter_values['post'][$to_query]))
            {
                $value = $this->filter_values['post'][$to_query];
            }else{
                throw new Exception('500','What are you trying to query?');
            }

        }else if( $name === 'get' ){

            if( isset($this->filter_values['get'][$to_query]) && !empty($this->filter_values['get'][$to_query]))
            {
                $value = $this->filter_values['get'][$to_query];
            }else{
                throw new Exception('500','What are you trying to query?');
            }

        }else if( $name === 'server' ){

            if( isset($this->filter_values['server'][$to_query]) && !empty($this->filter_values['server'][$to_query]))
            {
                $value = $this->filter_values['server'][$to_query];
            }else{
                throw new Exception('500','What are you trying to query?');
            }

        }else if( $name === 'cookie' ){

            if( isset( $this->filter_values['cookie'][$to_query]) && !empty($this->filter_values['cookie'][$to_query]))
            {
                $value = $this->filter_values['cookie'][$to_query];

            }else{
                throw new Exception('500','What are you trying to query?');
            }

        }else if( $name === 'request' ){

            if( isset( $this->filter_values['request'][$to_query] ) && !empty($this->filter_values['request'][$to_query]))
            {
                $value = $this->filter_values['request'][$to_query];

            }else{

                throw new Exception('500','What are you trying to query?');
            }

        }

        //TODO binding;

        return $value;
    }
}