<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 19/03/14
 * Time: 20:24
 */

namespace core\cache;


use core\CacheInterface;
use core\Config;

class Memory implements CacheInterface {

    private $is_cache_active = 0;
    private $cache;

    function __construct (  )
    {

        $this->is_cache_active = Config::getInstance()->getCache['active'];
        $this->cache =  new \Memcached();
        $this->cache->addserver( '127.0.0.1', 11211 );

    }

    public function set ( $key , $content , $expiration )
    {
        $this->cache->set( $key, $content, $expiration );
    }


    public function get ($key)
    {
        //todo check if has expired
        if( $this->is_cache_active === 0 )
        {
            return null;
        }
        return $this->cache->get( $key );

    }

    public function deleteKey ( $value )
    {
        return true;
    }

    public function getKey ( $parameters )
    {
        return sha1(serialize( $parameters ));
    }
} 