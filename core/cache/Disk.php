<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 19/03/14
 * Time: 19:31
 */

namespace core\cache;


use core\CacheInterface;
use core\Config;

class Disk implements  CacheInterface {


    private $is_cache_active = 0;

    function __construct (  )
    {
        $this->is_cache_active = Config::getInstance()->getCache['active'];
    }

    public function set ( $key , $content , $expiration )
    {
        $time = time() + $expiration;
        file_put_contents($this->getPath( $this->getKey($key) ),$time.$content);
    }

    public function get ($key)
    {
        //todo check if has expired , never expire
        if( $this->is_cache_active === 1 )
        {
            $path = $this->getPath($this->getKey($key));
            if(file_exists($path))
            {
                $cache_var = file_get_contents($path);

                $key_expiration_time = substr($cache_var ,0 , 10);

                $time = time();

                if( $key_expiration_time < $time)
                {
                    $this->deleteKey($this->getPath($this->getKey($key)));
                    return null;
                }
                return substr( $cache_var , 10);
            }
            return null;
        }
        return null;
    }

    public function deleteKey ( $path )
    {
        unlink($path);
    }
    public function getPath($key)
    {
        return "/var/www/ramdisk/$key.cache";
    }

    public function getKey ($parameters)
    {
        return sha1(serialize($parameters));
    }
} 