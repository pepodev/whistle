<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 19/03/14
 * Time: 19:31
 */

namespace core\cache;


use core\CacheInterface;
use core\Config;

abstract class Cache {

    private $key;
    private $content;
    private $expiration;
    private $is_cache_active = false;


    function __construct (  )
    {
        $this->is_cache_active = Config::getInstance()->getIsCache();
    }

    protected function set ( $key , $content , $expiration )
    {
        return true;
    }

    protected function get ($key)
    {
        if( $this->is_cache_active === false )
        {
            return null;
        }
        return null;
    }

    protected function deleteKey ( $value )
    {
        return true;
    }

    protected function getKey ($parameters)
    {
            return sha1(serialize($parameters));
    }
} 