<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 31/01/14
 * Time: 01:50
 */

namespace core;


abstract class Controller {


    protected $view;
    protected $cache = null;

    public function __construct( $view )
    {
        $this->setView($view);
    }

    public function setCache(CacheInterface $cache )
    {
        $this->cache = $cache;
    }
    public function execute()
    {
        if( $this->cache === null || method_exists( $this , "getControllerCacheDefinition" ) === false  )
        {
            $this->build();
            $html = $this->view->getHtml();

        }else{
            $cache_definition = $this->getControllerCacheDefinition();

            if($html = $this->cache->get($this->cache->getKey($cache_definition)))
            {
//                echo 'Html loaded from cache, class used:'.get_class($this->cache);

            }else{
                $this->build();
                $html = $this->view->getHtml();

                //todo check if controller is in cache method get cache is defined and if cache is actived at config

                $this->cache->set( $this->cache->getKey($cache_definition) ,$html, $cache_definition[1] );
            }
        }

        echo $html;
    }

    abstract protected function build();

    public function setView($view)
    {

        $this->view = $view;

    }

    public function renderTwig( $template , $vars )
    {

        $this->view->renderTwig( $template , $vars);

    }

    public function renderPhp( $template , $vars )
    {

        $this->view->assignPhp( $vars);

        $this->view->renderPhp( $template );

    }

    public function renderJson( array $array )
    {
        $this->view->renderJson( json_encode($array) );
    }

    public function getParams( )
    {

        $url_separator = Config::getInstance()->getUrl['url_separator'];

        $context_separator = Config::getInstance()->getUrl['context_separator'];

        $uri_parts = explode( $url_separator , $_SERVER['REQUEST_URI'] );

        array_shift($uri_parts);

        foreach($uri_parts AS $key => $param)
        {

            if( strstr( $param , $context_separator ) )
            {

                $param_parts = explode( $context_separator, $param);

                $params['param'] = $param_parts[0];

                array_shift($param_parts);

                foreach( $param_parts AS $param_key => $param_part)
                {

                    $params['param_parts'][$param_key] = $param_part;
                }

                $url_params['params'][$key] = $params;

            }else{

                $url_params['params'][$key] = $param;
            }
        }

        return $url_params;

    }

    public function redirect301( $route )
    {
        $environment = Config::getInstance()->getEnvironment;

        $filter = Filter::getInstance();

        $server_name = $filter->server( 'SERVER_NAME' );

        $newRoute =  'http://'. $server_name .'/'.$route;

        if( $environment==='dev' )
        {
            header ('HTTP/1.1 301 Moved Permanently');

            $html = <<<HTML
                    <h1>301 This page has been moved permanently</h1>
                    <p>please click the link below or wait 5 seconds</p>
                    <a href="{$newRoute}.">{$newRoute}</a>
HTML;

            $javscript = "<script language=\"javascript\" type=\"text/javascript\">window.setTimeout(function(){window.location.href = \"$newRoute\";}, 5000);</script>";

            echo $html;
            echo $javscript;

        }elseif( $environment==='prod' ){

            header( "Location:" . $newRoute ,true ,301);

        }

        die;

    }

    public function redirect302( $route )
    {

        $environment = Config::getInstance()->getEnvironment;

        $filter = Filter::getInstance();

        $server_name = $filter->server( 'SERVER_NAME' );

        $newRoute =  'http://'. $server_name .'/'.$route;

        if( $environment==='dev' )
        {
            header ('HTTP/1.1 302 Found');

            $html = <<<HTML
                    <h1>302 Found</h1>
                    <p>please click the link below or wait 5 seconds</p>
                    <a href="{$newRoute}.">{$newRoute}</a>
HTML;

            $javscript = "<script language=\"javascript\" type=\"text/javascript\">window.setTimeout(function(){window.location.href = \"$newRoute\";}, 5000);</script>";

            echo $html;
            echo $javscript;

        }elseif( $environment==='prod' ){

            header( "Location:" . $newRoute ,true ,302);

        }

        die;
    }
} 