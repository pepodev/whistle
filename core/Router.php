<?php
/**
 * Created by PhpStorm.
 * User: joseplluisgiraltdlacoste
 * Date: 27/01/14
 * Time: 20:56
 */

namespace core;


class Router {


    private $routes;
    private $routes301;

    public function __construct( )
    {
        $languages = Config::getInstance()->getLanguages;

        if((@include_once '../app/config/router.config.php') === false)
        {
            Throw new Exception('500', 'Failed including router config file.');
        }

        foreach( $languages AS $lang )
        {
            if((@include_once'../app/config/lang/' . $lang .'_router.php') === false)
            {
                Throw new Exception('500', 'Failed including '. $lang .' router config file.');
            }
        }

        if((@include_once'../app/config/301_rules.config.php') === false)
        {
            Throw new Exception('500', 'Failed including 301_rules config file.');
        }

        if((@include_once'../app/config/302_rules.config.php') === false)
        {
            Throw new Exception('500', 'Failed including 302_rules config file.');
        }

        $this->routes = $routes;
        $this->routes301 = $routes301;
        $this->routes302 = $routes302;

    }

    public function constructPath()
    {

        $url_separator = Config::getInstance()->getUrl['url_separator'];

        $uriParts = explode( $url_separator , $_SERVER['REQUEST_URI'] );

        array_shift($uriParts);

        if($uriParts[0] === '')
        {
            return $this->routes['__DEFAULT__'];
        }

        $environment = Config::getInstance()->getEnvironment;

        if( isset($this->routes[$uriParts[0]] ))
        {
            return $this->routes[$uriParts[0]];

        }else{

            if( isset($this->routes301[$uriParts[0]] ))
            {
                $newRoute =  'http://'.$_SERVER['SERVER_NAME'].'/'.$this->routes301[$uriParts[0]];


                if( $environment==='dev' )
                {
                    header ('HTTP/1.1 301 Moved Permanently');

                    $html = <<<HTML
                    <h1>301 This page has been moved permanently</h1>
                    <p>please click the link below or wait 5 seconds</p>
                    <a href="{$newRoute}.">{$newRoute}</a>
HTML;

                    $javscript = "<script language=\"javascript\" type=\"text/javascript\">window.setTimeout(function(){window.location.href = \"$newRoute\";}, 5000);</script>";

                    echo $html;
                    echo $javscript;

                }elseif( $environment==='prod' ){

                    header( "Location:" . $newRoute ,true ,301);

                }

                die;

            }else{
                if( isset($this->routes302[$uriParts[0]] ))
                {
                    $newRoute =  'http://'.$_SERVER['SERVER_NAME'].'/'.$this->routes302[$uriParts[0]];

                    if( $environment==='dev' )
                    {
                        header ('HTTP/1.1 302 Found');

                        $html = <<<HTML
                    <h1>302 Found</h1>
                    <p>please click the link below or wait 5 seconds</p>
                    <a href="{$newRoute}.">{$newRoute}</a>
HTML;

                        $javscript = "<script language=\"javascript\" type=\"text/javascript\">window.setTimeout(function(){window.location.href = \"$newRoute\";}, 5000);</script>";

                        echo $html;
                        echo $javscript;

                    }elseif( $environment==='prod' ){

                        header( "Location:" . $newRoute ,true ,302);

                    }

                    die;

                }else{
                    Throw new Exception('404','Route not found');
                }
            }
        }

    }

} 